class grandparent{                              //Base class
    grandparent(){System.out.println("Types of Inheritance in Java ");}
}

class parent extends grandparent{               //Single Inheritance
    parent(){System.out.println("Single Inheritance ");}
}

class son extends parent{}                      //Multiple Inheritance

class daughter extends parent{                  //Both Multiple and Hierarchical Inheritance
    daughter(){
        System.out.println("Multilevel Inheritance ");
        System.out.println("Hierarchical Inheritance ");
    }  
}
public class InheriTypeJava {
    public static void main(String[] args){
        daughter obj = new daughter();
    }
}



