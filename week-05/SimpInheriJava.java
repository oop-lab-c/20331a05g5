import java.util.*;
class base{                         //Base class                        
    void common(){
    System.out.println("English,Sanskrit,Physics,Chemisstry");
    }
} 
class MPC extends base{             //Simple inheritance
    void subjects(){
        System.out.println("Maths A,Maths B");
    }
}
class BIPC extends base{            //Simple inheritance
    void subjects(){
        System.out.println("Botany,Biology");
    }
}
public class SimpInheriJava {
    public static void main(String[] args) {
        MPC a=new MPC();
        BIPC b=new BIPC();
        System.out.println("MPC Subjects");
        a.common();             //Base method call
        a.subjects();           //Derived method call
        System.out.println("BIPC Subjects");
        b.common();
        b.subjects();
    }
}

