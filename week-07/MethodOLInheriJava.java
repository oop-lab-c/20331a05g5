class a
{
    void fun(int a, int b)
    {
        System.out.println(a+b);
    }
}
class MethodOLInheriJava extends a 
{
    void fun(int a, int b, int c)
    {
        System.out.println(a+b+c);
    }
    public static void main(String[] args)
    {
        MethodOLInheriJava obj = new MethodOLInheriJava();
        obj.fun(512,206);
        obj.fun(1,12,98);
    }
    
}

