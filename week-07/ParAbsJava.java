abstract class Impureabs{
    abstract void display();
}
class ParAbsJava extends Impureabs
{
    void display()
    {
        System.out.println("Partial Abstraction ");
    }
    public static void main(String[] args)
    {
        ParAbsJava obj = new ParAbsJava();
        obj.display();
    }
}
